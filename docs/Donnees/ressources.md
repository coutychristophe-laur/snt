---
author: à compléter
title: 📚 Ressources
---

# Données

## Introduction

!!! abstract "Présentation"

    Dans cette activité, on découvre le thème Données structurées à travers une vidéo tirée du Mooc SNT.



??? note "01. Introduction"

	<div class="centre">
	<iframe 
	src="../a_telecharger/01/01. Introduction - DONNEES STRUCTURE TRAITEMENT.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>



[01. Introduction](https://forge.apps.education.fr/ressources/snt/-/tree/main/docs/Donnees/a_telecharger/01){ .md-button target="_blank" rel="noopener" }


## Découverte du traitement de données

!!! abstract "Présentation"

    Dans cette activité, on découvre la thématique à travers une vidéo et un QCM. Ensuite on effectue de façon simple et graphique des tris sur un tableau de données. 


??? note "02. Découverte du traitement de données"

	<div class="centre">
	<iframe 
	src="../a_telecharger/02/02. Découverte du traitement de données - DONNEES STRUCTURE TRAITEMENT.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>

[Découverte du traitement de données](https://forge.apps.education.fr/ressources/snt/-/tree/main/docs/Donnees/a_telecharger/02){ .md-button target="_blank" rel="noopener" }

## Repères historiques

!!! abstract "Présentation"

    Dans cette activité, on présente quelques dates clefs sur les données structurées en relation des événements historiques concomitants.


??? note "03. Repères historiques"

	<div class="centre">
	<iframe 
	src="../a_telecharger/03/03. Repères historiques - DONNEES STRUCTURE TRAITEMENT.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>

[03. Repères historiques](https://forge.apps.education.fr/ressources/snt/-/tree/main/docs/Donnees/a_telecharger/03){ .md-button target="_blank" rel="noopener" }


## Traitement des données

!!! abstract "Présentation"

    Dans cette activité, on manipule des données ouvertes provenant du site data.gouv.fr afin d’en extraire de l’information pertinente.


??? note "04. Traitement"

	<div class="centre">
	<iframe 
	src="../a_telecharger/04/04. Traitement - DONNEES STRUCTURE TRAITEMENT.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>

[04. Traitement](https://forge.apps.education.fr/ressources/snt/-/tree/main/docs/Donnees/a_telecharger/04){ .md-button target="_blank" rel="noopener" }

## Données et formats

!!! abstract "Présentation"

    Dans cette activité, on présente le vocabulaire lié aux données structurées et on l’illustre par différents exemples.


??? note "05. Données et formats"

	<div class="centre">
	<iframe 
	src="../a_telecharger/05/05. Données et formats - DONNEES STRUCTURE TRAITEMENT.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>

[05. Données et formats](https://forge.apps.education.fr/ressources/snt/-/tree/main/docs/Donnees/a_telecharger/05){ .md-button target="_blank" rel="noopener" }

## Traitement de données ouvertes

!!! abstract "Présentation"

    Dans cette activité, l’enseignant.e explique la manipulation des données ouvertes provenant du site data.gouv.fr afin d’en extraire de l’information pertinente.
    Durant ce cours l’enseignant.e fait un suivi direct des élèves en situation de manipulation des informations à traiter.


??? note "06. Traitement de données ouvertes"

	<div class="centre">
	<iframe 
	src="../a_telecharger/06/06. Traitement de données ouvertes - DONNEES STRUCTURE TRAITEMENT.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>

[06. Traitement de données ouvertes](https://forge.apps.education.fr/ressources/snt/-/tree/main/docs/Donnees/a_telecharger/06){ .md-button target="_blank" rel="noopener" }

## Introduction aux données structurées

!!! abstract "Présentation"

    Dans cette activité, les élèves découvrent quelques types de données structurées. L’enseignant.e présente en particulier les données ouvertes liées au site data.gouv.fr, les données au format CSV et les données au format JSON et GeoJSONDurant.


??? note "07. Introduction données structurées"

	<div class="centre">
	<iframe 
	src="../a_telecharger/07/07. Introduction données structures- DONNEES STRUCTURE TRAITEMENT.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>

[Introduction données structurées](https://forge.apps.education.fr/ressources/snt/-/tree/main/docs/Donnees/a_telecharger/07){ .md-button target="_blank" rel="noopener" }

## Découverte des données

!!! abstract "Présentation"

    Dans cette activité, les élèves la thématique à travers une vidéo et un QCM. Ensuite les élèves effectuent de façon simple et graphique des tris sur un tableau de données.


??? note "08. Découverte des données"

	<div class="centre">
	<iframe 
	src="../a_telecharger/08/08. Données découvertes - DONNEES STRUCTURE TRAITEMENT.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>

[08. Données découverte](https://forge.apps.education.fr/ressources/snt/-/tree/main/docs/Donnees/a_telecharger/08){ .md-button target="_blank" rel="noopener" }

## CSV

!!! abstract "Présentation"

    Dans cette activité, les élèves on identifie les différents descripteurs d’un objet, on distingue la valeur d’une donnée de son descripteur, et on utilise un site de données ouvertes, pour sélectionner et récupérer des données découvrent quelques types de données structurées.


??? note "09. CSV"

	<div class="centre">
	<iframe 
	src="../a_telecharger/09/09. CSV - DONNEES STRUCTURE TRAITEMENT.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>

[09. CSV](https://forge.apps.education.fr/ressources/snt/-/tree/main/docs/Donnees/a_telecharger/09){ .md-button target="_blank" rel="noopener" }

## JSON

!!! abstract "Présentation"

    Dans cette activité, les élèves on identifie les différents descripteurs d’un objet, on distingue la valeur d’une donnée de son descripteur, et on utilise un site de données ouvertes, pour sélectionner et récupérer des données découvrent quelques types de données structurées.


??? note "10. JSON"

	<div class="centre">
	<iframe 
	src="../a_telecharger/10/10. JSON - DONNEES STRUCTURE TRAITEMENT.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>

[10. JSON](https://forge.apps.education.fr/ressources/snt/-/tree/main/docs/Donnees/a_telecharger/10){ .md-button target="_blank" rel="noopener" }